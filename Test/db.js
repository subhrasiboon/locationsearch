const modules = require("../Modules/main");
const Config = require("../Config/config.json")
const Db = modules.DB;

var Datastore = require('nedb'), testusers = new Datastore(), testlocations = new Datastore();
testusers.ensureIndex({ fieldName: 'email', unique: true });

let userid = "";

let user = {
    name: "test1",
    email: "abc@xyz",
    pwd: "pwd"
};

let location = {
    user: userid,
    coordinates: [12.182333, 13.89898],
    location: "abcplace",
    notes: "notes",
    placeid: "some place",
};

describe('Db', function () {

    it('It should create a user', function (done) {
        Db.createUser(testusers, user)
            .then(function (dbres) {
                if (dbres.email == user.email) {
                    userid = dbres._id;
                    done();
                }
            })
            .catch(function (err) {
                done(err);
            });
    });


    it('It should check if user exsists or not', function (done) {
        Db.userExsists(testusers, user.email)
            .then(function (truth) {
                if (truth) done()
            }).catch(function (err) {
                console.log(err);
            });
    });

    it('It should fetch details a user', function (done) {
        Db.getUser(testusers, user.email)
            .then(function (dbres) {

                if (dbres.name == user.name) done()
            })
            .catch(function (err) {
                done(err);
            });
    });

    it('It should be able to save users locations in to db', function (done) {
        Db.createLocation(testlocations, userid, location)
            .then(function (dbres) {
                if (dbres.name == location.name) done()
            }).catch(function (err) {
                console.log(err);
            });
    });

    it('It should fetch all the locations saved by a user', function (done) {
        Db.getLocationOfUser(testlocations, userid)
            .then(function (locations) {
                if (locations[0].name == location.name) done()
            })
            .catch(function (err) {
                done(err);
            });
    });

});
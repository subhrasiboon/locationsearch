const modules = require("../Modules/main");
const Auth = modules.Auth;


describe('Auth', function () {
    let secretPattern = "/^/";
    let token;
    let testData = "testData";

    it('should generate token', function (done) {
        token = Auth.genToken(secretPattern, testData);
        if (token.split(secretPattern).length == 2) done()
    });

    it('should decode correct token', function (done) {
        let verifiedData = Auth.verifyToken(secretPattern, token);
        if (verifiedData == testData) done()

    });

    it('should not decode wrong token', function (done) {
        try {
            let verifiedData = Auth.verifyToken(secretPattern, "wrong token");
            if (!verifiedData) done();
        } catch (err) {
            console.log(err);
        }
    });

});

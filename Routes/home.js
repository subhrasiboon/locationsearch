const express = require('express')
const modules = require("../Modules/main");
const Config = require("../Config/config.json");
const Db = modules.DB;
const Auth = modules.Auth;
const fetch = require('node-fetch');

const placeUrl = `https://maps.googleapis.com/maps/api/geocode/json?&key=${Config.maps_api_key}&place_id=`;

const router = express.Router();
module.exports = function (locationsdb) {

  // define the home page route
  router.get('/', function (req, res) {
    const token = req.cookies.token || "";
    const data = Auth.verifyToken(Config.sec_pattern, token);
    if (data) {

      Db.getCountLocationsUser(locationsdb, data.userid).then(count => {
        res.render('home.html', { total: count });
      }).catch(err => {
        res.render('home.html', { err: "Please try Again!" });
      })

    } else {
      res.redirect("/auth");
    }

  });

  router.post('/', function (req, res) {

    const token = req.cookies.token || "";
    const data = Auth.verifyToken(Config.sec_pattern, token);
    if (data) {

      let location = {
        name: req.body.InputLocation,
        coordinates: [],
        notes: req.body.InputNotes,
        placeid: req.body.InputPlaceId
      };

      if (!location.placeid) {

        Db.getCountLocationsUser(locationsdb, data.userid).then(count => {
          res.render('home.html', { total: count, err: "Please select a proper location" });
        }).catch(err => {
          res.render('home.html', { err: "Please Try again!" });
        })

      } else {

        fetch(placeUrl + location.placeid).then(function (response) {
          return response.json();
        }).then(function (json) {

          location.coordinates[0] = json.results[0].geometry.location.lat;
          location.coordinates[1] = json.results[0].geometry.location.lng;

          Db.createLocation(locationsdb, data.userid, location)
            .then(function (dbres) {

              Db.getCountLocationsUser(locationsdb, data.userid).then(count => {
                res.render('home.html', { total: count });
              }).catch(err => {
                res.render('home.html', { err: "Please Try again!" });
              })

            }).catch(err => {
              res.render('home.html', { err: "Please Try again!" });
            })

        })


      }

    } else {
      res.redirect("/auth");
    }

  });

  return router;

}
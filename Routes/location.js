const express = require('express')
const modules = require("../Modules/main");

const Db = modules.DB;
const Auth = modules.Auth;
const Config = require("../Config/config.json");

const fetch = require('node-fetch');
const gUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode&key=${Config.maps_api_key}&input=`;

const router = express.Router()
module.exports = function (locationsdb) {


  // define the home page route
  router.get('/', function (req, res) {
    const token = req.cookies.token || "";
    const data = Auth.verifyToken(Config.sec_pattern, token);

    if (data) {

      Db.getLocationOfUser(locationsdb, data.userid)
        .then(locations => {

          res.render('locations.html', { locations: locations });
        }).catch(err => res.render(err))

    } else {
      res.redirect("/auth");
    }


  });

  router.get('/map', function (req, res) {
    const token = req.cookies.token || "";
    const data = Auth.verifyToken(Config.sec_pattern, token);

    if (data) {
      let place = req.query.input

      fetch(gUrl + place).then(function (response) {
        return response.json();
      }).then(function (json) {
        let places = [];
        for (i of json["predictions"]) {
          places.push({ description: i.description, place_id: i.place_id })
        }
        res.send(places);
      })

    } else {
      res.redirect("/auth");
    }



  });

  return router;

}
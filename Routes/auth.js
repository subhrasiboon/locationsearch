var express = require('express')
var router = express.Router()
const modules = require("../Modules/main");
const Config = require("../Config/config.json");
const Db = modules.DB;
const Auth = modules.Auth;
const sha1 = require("sha1");

module.exports = function (userdb) {

  // middleware that is specific to this router
  router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now())
    next()
  });


  router.get('/', function (req, res) {
    let data = {};
    res.render('login.html', data);
  });

  router.get('/signout', function (req, res) {
    let data = {};
    res.cookie('token', "").redirect('/auth');
  });

  router.post('/', function (req, res) {
    // res.cookie('name', 'express').send('cookie set');
    let user = {
      email: req.body.InputEmail,
      pwd: req.body.InputPassword
    };



    Db.getUser(userdb, user.email)
      .then(dbres => {
        if (dbres.pwd == sha1(user.pwd)) {
          const token = Auth.genToken(Config.sec_pattern, { userid: dbres._id });
          res.cookie('token', token).redirect('/');
        } else {
          res.render('login.html', { err: "Username or Password Wrong", email: user.email });
        }
      }).catch(err => {
        res.render('login.html', { err: "Username or Password Wrong", email: user.email });
      })

  });


  return router;

}
const express = require('express')
const router = express.Router()
const modules = require("../Modules/main");
const Config = require("../Config/config.json");
const Db = modules.DB;
const Auth = modules.Auth;

module.exports = function (userdb) {



  // define the home page route
  router.get('/', function (req, res) {
    let data = {};
    res.render('signup.html', data);
  });

  router.post('/', function (req, res) {

    let user = {
      name: req.body.InputUsername,
      email: req.body.InputEmail,
      pwd: req.body.InputPassword
    };
    // res.cookie('name', 'express').send('cookie set');

    Db.createUser(userdb, user)
      .then(dbres => {
        const token = Auth.genToken(Config.sec_pattern, { userid: dbres._id });
        // console.log(Auth.verifyToken(Config.sec_pattern,token));
        res.cookie('token', token).redirect('/');
        // res.send(token);
      }).catch(err => {
        res.render('signup.html', { err: "User may aleardy exsist!" });
      });

  });

  return router;

}

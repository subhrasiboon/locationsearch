const express = require('express');
const bodyParser = require('body-parser');
const status = require("http-status");
const app = express();
const nunjucks = require( 'nunjucks' ) ;
const cookieParser = require('cookie-parser')
const Config = require("./Config/config.json");

const modules = require("./Modules/main");
const Auth = modules.Auth;

const Datastore = require('nedb'); 
userdb = new Datastore();
locationsdb = new Datastore();
userdb.ensureIndex({ fieldName: 'email', unique: true });

app.use(cookieParser())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const PATH_TO_TEMPLATES = './Views' ;
nunjucks.configure( PATH_TO_TEMPLATES, {
    autoescape: true,
    express: app
} ) ;

//ROUTES
const login = require('./Routes/auth')(userdb);
const signup = require('./Routes/signup')(userdb);
const location = require('./Routes/location')(locationsdb);
const home = require('./Routes/home')(locationsdb);

app.use('/auth', login);
app.use('/signup', signup);
app.use('/location', location);
app.use('/', home);

module.exports = app;

console.log(`Runnig at ${Config.port}`);
app.listen(Config.port);



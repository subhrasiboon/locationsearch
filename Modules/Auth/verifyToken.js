var jwt = require('jsonwebtoken');

function verifyToken(secret_pattern, token) {

    let sec = token.split(secret_pattern)[1];
    let decoded;

    //verify the token
    try {
        decoded = jwt.verify(token.split(secret_pattern)[0], sec);
        return decoded.data;
    } catch (err) {
        return false;
    }

}
module.exports = verifyToken;


// app.post('/webhook', function (req, res) {
//   let token = req.query.token;

//   let verified = Auth.verifyToken(secret_pattern,token);

//   if(verified){
//     let json = req.body.message || req.body.edited_message;
//     bot(json);
//     res.status(status.OK).send("");
//   }else{
//     res.status(status.BAD_REQUEST).send("");
//   }

// });

var sha1 = require("sha1");

function createUser(db, user) {
    return new Promise(function (resolve, reject) {
        db.count({
            email: user.email,
        }, function (err, count) {
            if (err) reject(err)
            else if (!count) {
                db.insert({
                    email: user.email,
                    name: user.name,
                    pwd: sha1(user.pwd),
                }, function (err, docs) {
                    if (err) reject(err);
                    resolve(docs);
                });
            } else {
                reject("User Alerdy Exsists")
            }
        });


    })

}

module.exports = createUser;

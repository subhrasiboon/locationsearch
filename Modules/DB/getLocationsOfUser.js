function getLocationsOfUser(db, userid) {
    return new Promise(function (resolve, reject) {
        db.find({ user: userid }, function (err, doc) {
            if (err) reject(err)
            resolve(doc);
        });
    })

}

module.exports = getLocationsOfUser;
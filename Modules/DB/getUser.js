function getUser(db, email) {
    return new Promise(function (resolve, reject) {
        db.findOne({ email: email }, function (err, doc) {
            if (err) reject(err)
            resolve(doc);
        });
    })

}

module.exports = getUser;
function getCountLocationsUser(db, userid) {
    return new Promise(function (resolve, reject) {
        db.count({ user: userid }, function (err, doc) {
            if (err) reject(err)
            resolve(doc);
        });
    })

}

module.exports = getCountLocationsUser;
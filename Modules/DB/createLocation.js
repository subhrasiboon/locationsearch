function createLocation(db, userid, location) {
    return new Promise(function (resolve, reject) {
        db.insert({
            user: userid,
            coordinates: location.coordinates,
            location: location.name,
            notes: location.notes,
            placeid: location.placeid,
        }, function (err, docs) {
            if (err) reject(err);
            resolve(docs);
        });
    })
}

module.exports = createLocation;

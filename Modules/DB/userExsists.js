function userExsists(db, email) {
    return new Promise(function (resolve, reject) {
        db.count({ email: email }, function (err, count) {
            if (err) reject(err)
            resolve(count > 0);
        });
    })

}

module.exports = userExsists;
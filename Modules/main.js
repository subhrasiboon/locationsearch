
//DB
const createUser = require("./DB/createUser");
const userExsists = require("./DB/userExsists");
const getUser = require("./DB/getUser");
const createLocation = require("./DB/createLocation");
const getLocationOfUser = require("./DB/getLocationsOfUser");
const getCountLocationsUser = require("./DB/getCountLocationsUser");

//Auth
const genToken = require("./Auth/genToken");
const verifyToken = require("./Auth/verifyToken");

module.exports = {
    Auth: {
        genToken: genToken,
        verifyToken: verifyToken,

    },

    DB: {
        createUser: createUser,
        userExsists: userExsists,
        getUser: getUser,
        createLocation: createLocation,
        getLocationOfUser: getLocationOfUser,
        getCountLocationsUser: getCountLocationsUser
    }
}